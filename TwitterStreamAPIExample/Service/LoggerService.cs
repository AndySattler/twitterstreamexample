﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Linq;
using TwitterStreamAPIExample.Models;

namespace TwitterStreamAPIExample.Services
{
    public class LoggerService : Interfaces.ILoggerService
    {
        readonly List<TweetErrorDTO> tweetErrorlogs;
        readonly List<ErrorMessageDTO> errorMessages;
        public LoggerService()
        {
            tweetErrorlogs = new List<TweetErrorDTO>();
            errorMessages = new List<ErrorMessageDTO>();
        }


        public void Log(TweetErrorDTO data)
        {
            Console.WriteLine($"{data.Exception} | {data.Tweet} | {data.Time}");
            tweetErrorlogs.Add(data);
        }

        public void Log(ErrorMessageDTO errorMessage)
        {
            Console.WriteLine($"{errorMessage.ErrorMessage} | {errorMessage.Exception}");
            errorMessages.Add(errorMessage);
        }
    }
}
