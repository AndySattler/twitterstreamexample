﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Linq;

namespace TwitterStreamAPIExample.Services
{
    public class EmojiService : Interfaces.IEmojiService
    {
        public EmojiService()
        {
        }

        public async Task<string[]> LoadData(string fileLocation)
        {
            using (StreamReader sr = new StreamReader(fileLocation))
            {
                string jsonString = await sr.ReadToEndAsync();
                Models.EmojiJsonDTO[] jsonDTOs = JsonConvert.DeserializeObject<Models.EmojiJsonDTO[]>(jsonString);

                return jsonDTOs.Select(p=>p.unified).ToArray();
            }
        }



    }
}
