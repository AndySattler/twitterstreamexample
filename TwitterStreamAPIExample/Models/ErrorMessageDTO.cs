﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwitterStreamAPIExample.Models
{
    public class ErrorMessageDTO
    {
        public DateTime Time { get; set; }
        public LogLevel Level { get; set; }
        public Exception Exception { get; set; }       
        public string ErrorMessage { get; set; }
    }
}
