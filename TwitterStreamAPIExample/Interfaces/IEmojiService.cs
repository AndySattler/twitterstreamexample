﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace TwitterStreamAPIExample.Interfaces
{
    public interface IEmojiService
    {
        Task<string[]> LoadData(string fileLocation);

    }
}
