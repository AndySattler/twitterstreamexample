﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using NeoSmart.Unicode;

namespace TwitterStreamAPIExample
{
    public class Statistics
    {
        DateTime startTime;
        int tweetCount;
        readonly Dictionary<string, int> hashtagCount;
        int tweetContainsUrlCount;
        int tweetContainsPhotoCount;
        readonly Dictionary<string, int> emojiCount;
        int tweetContainsEmojiCount;
        readonly Dictionary<string, int> topDomainsInUrl;
        readonly int numberOfResultsForTop;

        HashSet<string> emojis;

        public Statistics(IEnumerable<string> emojiCharacters,int numberOfResults)
        {
            startTime = DateTime.Now;
            hashtagCount = new Dictionary<string, int>();
            emojiCount = new Dictionary<string, int>();
            emojis = new HashSet<string>(emojiCharacters);
            topDomainsInUrl = new Dictionary<string, int>();
            numberOfResultsForTop = numberOfResults;
        }

        public int GetTweetCount => tweetCount;

        public void AddTweet(Tweetinvi.Models.ITweet tweet)
        {
            bool containsEmoji = false;
            tweetCount++;

            foreach (Tweetinvi.Models.Entities.IHashtagEntity hashtag in tweet.Hashtags)
            {
                if (hashtagCount.ContainsKey(hashtag.Text))
                {
                    hashtagCount[hashtag.Text]++;
                }
                else
                {
                    hashtagCount.Add(hashtag.Text, 1);
                }
            }

            string[] words = tweet.Text.Split(' ');
            bool tweetContainsUrl = false;

            foreach (string word in words)
            {

                if (word.StartsWith("https://") || word.StartsWith("http://"))
                {                   
                    UriBuilder uri = new UriBuilder(word);

                    if(topDomainsInUrl.ContainsKey(uri.Host))
                    {
                        topDomainsInUrl[uri.Host]++;
                    }
                    else
                    {
                        topDomainsInUrl.Add(uri.Host, 1);
                    }
                        
                    tweetContainsUrl=true;                   
                }
            }

            if (tweetContainsUrl)
                tweetContainsUrlCount++;

            if (tweet.Text.Contains("pic.twitter.com") || tweet.Text.Contains("www.instagram.com"))
            {
                tweetContainsPhotoCount++;
            }

            //emoji
            foreach(Codepoint codepoint in tweet.Text.Codepoints())
            {
                string code = codepoint.AsUtf32.ToString("X");
                if (emojis.Contains(code))
                {
                    containsEmoji = true;
                    if (emojiCount.ContainsKey(code))
                    {
                        emojiCount[code]++;
                    }
                    else
                    {
                        emojiCount.Add(code, 1);
                    }
                }
            }

            //tweetcontains emoji
            if (containsEmoji)
                tweetContainsEmojiCount++;
        }

        public string WriteStats()
        {
            TimeSpan runningTime = DateTime.Now - startTime;

            StringBuilder sb = new StringBuilder();

            //Total number of tweets received
            sb.AppendLine($"Total Tweets Recorded: {tweetCount}");

            //Average tweets per hour/ minute / second
            sb.AppendLine($"Tweets per hour: {tweetCount / runningTime.TotalHours}");
            sb.AppendLine($"Tweets per minute: {tweetCount / runningTime.TotalMinutes}");
            sb.AppendLine($"Tweets per second: {tweetCount / runningTime.TotalSeconds}");

            //Top emojis in tweets 
            sb.AppendLine($"Top {numberOfResultsForTop } emojis in tweets:");
            foreach (KeyValuePair<string, int> emoji in emojiCount.OrderByDescending(p => p.Value).Take(numberOfResultsForTop))
            {
                sb.AppendLine($"emoji : {emoji.Key}  count: {emoji.Value}");
            }

            //Percent of tweets that contains emojis
            sb.AppendLine($"Percent of tweets that contain emojis: { ((decimal)tweetContainsEmojiCount / (decimal)tweetCount) * 100 }%");

            //Top hashtags
            sb.AppendLine($"Top {numberOfResultsForTop } hashtags in tweets:");
            foreach (KeyValuePair<string, int> hashtag in hashtagCount.OrderByDescending(p => p.Value).Take(numberOfResultsForTop))
            {
                sb.AppendLine($"hashtag: {hashtag.Key}  count: {hashtag.Value}");
            }

            //Percent of tweets that contain a url
            sb.AppendLine($"Percent of tweets that contain a url: { ((decimal)tweetContainsUrlCount / (decimal)tweetCount) * 100}%");

            //Top Urls
            sb.AppendLine($"Top {numberOfResultsForTop} domains in urls");
            foreach (KeyValuePair<string, int> domain in topDomainsInUrl.OrderByDescending(p => p.Value).Take(numberOfResultsForTop))
            {
                sb.AppendLine($"domain: {domain.Key}  count: {domain.Value}");
            }

            //Percent of tweets that contain a photo url(pic.twitter.com or instagram)Top domains of urls in tweets
            sb.AppendLine($"Percent of tweets that contain a photo url: { ((decimal)tweetContainsPhotoCount / (decimal)tweetCount) * 100}%");

            return sb.ToString();
        }

    }
}
