﻿public class AppSettings
{
    public int StatisticsFrequency { get; set; }
    public string ConsumerKey { get; set; }
    public string ConsumerSecret { get; set; }
    public string AccessToken { get; set; }
    public string AccessSecret { get; set; }

}