﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwitterStreamAPIExample.Models
{
    public enum LogLevel { DEBUG, ERROR, INFO} 

    public class TweetErrorDTO
    {
        public DateTime Time { get; set; }
        public Exception Exception { get; set; }
        public LogLevel Level { get; set; }        
        public Tweetinvi.Models.ITweet Tweet { get; set; }
    }
}
