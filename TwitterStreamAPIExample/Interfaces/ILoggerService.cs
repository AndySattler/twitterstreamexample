﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Linq;
using TwitterStreamAPIExample.Models;

namespace TwitterStreamAPIExample.Interfaces
{
    public interface ILoggerService
    {
        void Log(TweetErrorDTO loggingDTO);
        void Log(ErrorMessageDTO errorMessage);

    }
}
