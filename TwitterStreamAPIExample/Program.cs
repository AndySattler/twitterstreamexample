﻿using System;
using Tweetinvi;
using System.Threading.Tasks;
using Tweetinvi.Exceptions;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using TwitterStreamAPIExample.Interfaces;
using TwitterStreamAPIExample.Services;
using Microsoft.Extensions.Configuration;
using System.IO;


namespace TwitterStreamAPIExample
{
    class Program
    {
        static Statistics stats;
        
        static async Task Main(string[] args)
        {            
            IEmojiService emojiService = new EmojiService();
            ILoggerService loggerService = new LoggerService();
            IEnumerable<string> emojis = await emojiService.LoadData(AppDomain.CurrentDomain.BaseDirectory + @"/Data/emoji.json");

            stats = new Statistics(emojis,5);
            AppSettings appSettings = new AppSettings();

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = builder.Build();
            ConfigurationBinder.Bind(configuration.GetSection("AppSettings"), appSettings);

            //this will update the statistics to the console every 200 tweets
            int statisticsFrequency = appSettings.StatisticsFrequency;
            int nextUpdateStatsNumber = statisticsFrequency;

            if (statisticsFrequency == 0)
            {
                loggerService.Log(new Models.ErrorMessageDTO() { 
                    ErrorMessage = "Missing the statistics frequency",
                    Level = Models.LogLevel.ERROR,
                    Time = DateTime.Now
                });
                return;
            }

            //we could also load these keys from environment variables if that is preferred
            if (string.IsNullOrWhiteSpace(appSettings.ConsumerKey) ||
                string.IsNullOrWhiteSpace(appSettings.ConsumerSecret) ||
                string.IsNullOrWhiteSpace(appSettings.AccessToken) ||
                string.IsNullOrWhiteSpace(appSettings.AccessSecret))
            {
                loggerService.Log(
                    new Models.ErrorMessageDTO()
                    {
                        ErrorMessage = "Missing one of the keys",
                        Level = Models.LogLevel.ERROR,
                        Time = DateTime.Now
                    });
                return;
            }

            var userClient = new TwitterClient(appSettings.ConsumerKey,
                                               appSettings.ConsumerSecret,
                                               appSettings.AccessToken,
                                               appSettings.AccessSecret);


            var sampleStream = userClient.Streams.CreateSampleStream();
            sampleStream.TweetReceived += (sender, eventArgs) =>
            {
                try
                {
                    stats.AddTweet(eventArgs.Tweet);
                    if (stats.GetTweetCount >= nextUpdateStatsNumber)
                    {
                        Console.Clear();
                        Console.WriteLine(stats.WriteStats());
                        nextUpdateStatsNumber = stats.GetTweetCount + statisticsFrequency;
                    }
                }
                catch (Exception ex)
                {
                    loggerService.Log(new Models.TweetErrorDTO()
                    {
                        Exception = ex,
                        Level = Models.LogLevel.ERROR,
                        Time = DateTime.Now,
                        Tweet = eventArgs.Tweet
                    });
                }
            };


            try
            {
                await sampleStream.StartAsync();
            }
            catch(Exception ex)
            {
                loggerService.Log(new Models.ErrorMessageDTO()
                {
                    ErrorMessage = "Error with the twitter stream",
                    Exception = ex,
                    Level = Models.LogLevel.ERROR,
                    Time = DateTime.Now,
                });
            }

        }
    }
}
